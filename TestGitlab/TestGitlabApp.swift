//
//  TestGitlabApp.swift
//  TestGitlab
//
//  Created by Joost Kemink on 11/12/21.
//

import SwiftUI

@main
struct TestGitlabApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
